from flask import request, Blueprint
from scripts.handler.utilities import Utilities

data = Blueprint("Data", __name__)

utility_obj = Utilities()


@data.route('/', methods=['POST'])
def postjsonhandler():
    content = request.get_json()
    utility_obj.data_parsing(content)
    return str(content)


@data.route('/find', methods=['GET'])
def find():
    result1 = utility_obj.data_finding()
    return str(result1)


@data.route('/delete', methods=['GET'])
def delete():
    result1 = utility_obj.data_deletion()
    return str(result1)


@data.route('/findone', methods=['GET'])
def findone():
    result1 = utility_obj.data_finding_one()
    return str(result1)


@data.route('/update', methods=['GET'])
def update():
    result1 = utility_obj.data_updating()
    return str(result1)

# print("Select action to be performed")
# print("1.Find\n 2.Update\n 3.Delete\n 4.Find_one")
# option = int(input())
# if option == 1:
#     utility_obj.data_finding()
# if option == 2:
#     utility_obj.data_updating()
# if option == 3:
#     utility_obj.data_deleting()
# if option == 4:
#     utility_obj.data_finding_one()
