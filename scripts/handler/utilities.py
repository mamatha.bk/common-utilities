from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client["mamatha"]
Collection = db["user_details351"]


class Utilities:
    @staticmethod
    def data_parsing(data):
        if isinstance(data, list):
            Collection.insert_many(data)
            print("Inserted")
        else:
            Collection.insert_one(data)
            print("Inserted")

    @staticmethod
    def data_finding():
        result = []
        for data1 in Collection.find({}, {"gender": 1}):
            print(data1)
            result.append(data1)
        return result

    @staticmethod
    def data_finding_one():
        one = Collection.find_one()
        print(one)
        return one

    @staticmethod
    def data_updating():
        myquery = {"gender": "male"}
        newvalues = {"$set": {"gender": "Male"}}
        Collection.update_many(myquery, newvalues)
        result = []
        for data in Collection.find({}, {"gender": 1}):
            print(data)
            result.append(data)
        return result

    @staticmethod
    def data_deletion():
        myquery = {"first_name": "Vassili"}
        result = []
        Collection.delete_one(myquery)
        for data in Collection.find({}, {"first_name": 1}):
            print(data)
            result.append(data)
        return result
