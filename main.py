from flask import Flask
from scripts.service.dataservice import data

app = Flask(__name__)
app.register_blueprint(data)
if __name__ == '__main__':
    app.run()
